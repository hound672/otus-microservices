1. Create namespace
kubectl create namespace otus

2. Accept secrets
kubectl apply -f secrets.yaml

3. Acceptc configs
kubectl apply -f config.yaml

4. Install DB
helm -n otus install psql /tmp/psql-hel,m/postgresql -f values.yaml --dry-run

5. Accept migration
kubectl apply -f backend/migrations.yaml

6. Run backend
kubectl apply -f backend/

package repositories

import (
	"context"
)

type UoW interface {
	Start(ctx context.Context) error
	Done(ctx context.Context, commit bool)
}

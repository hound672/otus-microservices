package repositories

import (
	"context"
	"errors"

	"gitlab.com/hound672/otus-microservices/06/internal/models"
)

var (
	ErrUserNotFound = errors.New("error user not found")
)

type User interface {
	Add(ctx context.Context, user *models.User) (*models.User, error)
	ReadAll(ctx context.Context) ([]*models.User, error)
	ReadByID(ctx context.Context, userID int) (*models.User, error)
	Update(ctx context.Context, user *models.User) error
	Delete(ctx context.Context, user *models.User) error
}

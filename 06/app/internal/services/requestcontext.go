package services

import "context"

type RequestContext interface {
	NewContext(ctx context.Context) context.Context
	Teardown(ctx context.Context)
}

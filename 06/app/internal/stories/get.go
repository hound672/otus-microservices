package stories

import (
	"context"

	"gitlab.com/hound672/otus-microservices/06/internal/models"
	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
)

type Get struct {
	repo repositories.User
}

func NewGet(repo repositories.User) *Get {
	get := &Get{
		repo: repo,
	}
	return get
}

func (s *Get) Handle(ctx context.Context) ([]*models.User, error) {
	users, err := s.repo.ReadAll(ctx)
	if err != nil {
		return nil, err
	}

	return users, nil
}

package stories

import (
	"context"

	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
)

type Change struct {
	repo repositories.User
}

func NewChange(repo repositories.User) *Change {
	change := &Change{
		repo: repo,
	}
	return change
}

func (s *Change) Handle(ctx context.Context, userID int, username, email, phone string) error {
	user, err := s.repo.ReadByID(ctx, userID)
	if err != nil {
		return err
	}

	user.Username = username
	user.Email = email
	user.Phone = phone

	err = s.repo.Update(ctx, user)
	if err != nil {
		return err
	}

	return nil
}

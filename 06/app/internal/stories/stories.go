package stories

type Stories struct {
	*Create
	*Get
	*Change
	*Delete
}

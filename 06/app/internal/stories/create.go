package stories

import (
	"context"
	"gitlab.com/hound672/otus-microservices/06/internal/models"

	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
)

type Create struct {
	repo repositories.User
}

func NewCreate(repo repositories.User) *Create {
	create := &Create{
		repo: repo,
	}
	return create
}

func (s *Create) Handle(ctx context.Context, username, email, phone string) error {
	user := &models.User{
		Username: username,
		Email:    email,
		Phone:    phone,
	}

	user, err := s.repo.Add(ctx, user)
	if err != nil {
		return err
	}

	return nil
}


package stories

import (
	"context"
	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
)

type Delete struct {
	repo repositories.User
}

func NewDelete(repo repositories.User) *Delete {
	deleteS := &Delete{
		repo: repo,
	}
	return deleteS
}

func (s *Delete) Handle(ctx context.Context, userID int) error {
	user, err := s.repo.ReadByID(ctx, userID)
	if err != nil {
		return err
	}

	err = s.repo.Delete(ctx, user)
	if err != nil {
		return err
	}

	return nil
}

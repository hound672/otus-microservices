//go:build wireinject
// +build wireinject

package application

import (
	"context"
	"sync"

	"github.com/google/wire"
)

func initApp(ctx context.Context, wg *sync.WaitGroup, configFile string) (*Application, func(), error) {
	panic(wire.Build(
		NewRequestContext,
		psqlConnection,
		initConfig,
		initLogger,
		initHTTPServer,
		controllersConstructorsSet,
		storiesSet,
		repositoriesSet,
		wire.Struct(new(Application), "*"),
	))
}

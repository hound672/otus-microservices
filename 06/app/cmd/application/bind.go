// nolint
package application

import (
	"github.com/google/wire"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/controllers"
	"gitlab.com/hound672/otus-microservices/06/cmd/postgres"
	repositories2 "gitlab.com/hound672/otus-microservices/06/cmd/postgres/repositories"
	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
	"gitlab.com/hound672/otus-microservices/06/internal/stories"

	"gitlab.com/hound672/otus-microservices/06/cmd/config"
	"gitlab.com/hound672/otus-microservices/06/cmd/http"
	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
)

// config

var initConfig = wire.NewSet(
	config.InitConfig,
)

// logger

var initLogger = wire.NewSet(
	wire.FieldsOf(new(*config.AppConfig), "Logger"),
	logger.InitLogger,
)

// http server
var initHTTPServer = wire.NewSet(
	http.InitHTTPServer,
)

var psqlConnection = wire.NewSet(
	wire.FieldsOf(new(*config.AppConfig), "Postgres"),
	postgres.InitPsql,
	postgres.NewConnectionManager,
	wire.NewSet(
		postgres.NewPsqlUoW,
		wire.Bind(new(repositories.UoW), new(*postgres.PsqlUoW)),
	),
)

var controllersConstructorsSet = wire.NewSet(
	wire.Struct(new(controllers.Controllers), "*"),
	controllers.NewErrorHandler,
	controllers.NewCreate,
	controllers.NewGet,
	controllers.NewChange,
	controllers.NewDelete,
)

// repositories

var repositoriesSet = wire.NewSet(
	repositories2.NewUser,
)

// stories

var storiesSet = wire.NewSet(
	wire.Struct(new(stories.Stories), "*"),
	stories.NewCreate,
	stories.NewGet,
	stories.NewChange,
	stories.NewDelete,
)

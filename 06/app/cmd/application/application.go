package application

import (
	"context"
	"fmt"
	"net/http"
	"sync"

	"github.com/labstack/echo/v4"

	"gitlab.com/hound672/otus-microservices/06/cmd/config"
	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
)

const configFile = "config.yml"

type Application struct {
	wg *sync.WaitGroup
	*echo.Echo
	logger.Logger
	AppConfig *config.AppConfig
}

func CreateApplication() (*Application, func(), error) {
	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	app, cleanup, err := initApp(ctx, wg, configFile)
	if err != nil {
		cancel()
		return nil, nil, err
	}

	stopServer := func() {
		app.Logger.Infof("Stop application.")
		cancel()
		cleanup()
		wg.Wait()
		app.Logger.Debugf("All done")
	}

	return app, stopServer, err
}

func (app *Application) Run() error {
	app.Logger.Infof("Start Otus Service")

	address := fmt.Sprintf(":%d", app.AppConfig.HTTPServer.Port)
	app.wg.Add(1)
	go func() {
		if err := app.Echo.Start(address); err != nil && err != http.ErrServerClosed {
			app.Logger.Fatalf("Error start http server: %s", err)
		}
		app.wg.Done()
	}()

	return nil
}

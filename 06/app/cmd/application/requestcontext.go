package application

import (
	"context"

	"gitlab.com/hound672/otus-microservices/06/cmd/postgres"
	"gitlab.com/hound672/otus-microservices/06/cmd/tools"
	"gitlab.com/hound672/otus-microservices/06/internal/services"
)

func NewRequestContext(connectionManager *postgres.ConnectionManager) services.RequestContext {
	requestContext := &requestContext{
		connectionManager: connectionManager,
	}
	return requestContext
}

type requestContext struct {
	connectionManager *postgres.ConnectionManager
}

func (c *requestContext) NewContext(ctx context.Context) context.Context {
	newCtx := tools.NewRequestContext(ctx)
	return newCtx
}

func (c *requestContext) Teardown(ctx context.Context) {
	c.connectionManager.Teardown(ctx)
}

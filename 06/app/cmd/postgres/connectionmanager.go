package postgres

import (
	"context"
	"sync"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
	"gitlab.com/hound672/otus-microservices/06/cmd/tools"
)

type ConnectionManager struct {
	log         logger.Logger
	connections sync.Map
	pool        *pgxpool.Pool
}

func NewConnectionManager(pool *pgxpool.Pool, log logger.Logger) (*ConnectionManager, func(), error) {
	manager := &ConnectionManager{
		log:  log,
		pool: pool,
	}

	cleanup := func() {
		manager.cleanup()
	}

	return manager, cleanup, nil
}

func (c *ConnectionManager) GetConnection(ctx context.Context) (*pgxpool.Conn, error) {
	reqID := tools.GetRequestID(ctx)

	if v, ok := c.connections.Load(reqID); ok {
		conn, ok := v.(*pgxpool.Conn)
		if !ok {
			c.log.Errorf("Error cast to *pgxpool.Conn in GetConnection ConnectionManager")
			return nil, nil // TODO: Add error
		}
		return conn, nil
	}

	conn, err := c.pool.Acquire(ctx)
	if err != nil {
		return nil, err
	}
	c.connections.Store(reqID, conn)
	return conn, nil
}

func (c *ConnectionManager) Teardown(ctx context.Context) {
	reqID := tools.GetRequestID(ctx)

	v, ok := c.connections.Load(reqID)
	if !ok {
		return
	}
	conn, ok := v.(*pgxpool.Conn)
	if !ok {
		c.log.Errorf("Error cast to *pgxpool.Conn in Teardown ConnectionManager")
		return
	}
	conn.Release()
}

func (c *ConnectionManager) cleanup() {
	c.connections.Range(func(_ interface{}, v interface{}) bool {
		conn, ok := v.(*pgxpool.Conn)
		if !ok {
			c.log.Errorf("Error cast to *pgxpool.Conn in cleanup ConnectionManager")
			return false
		}

		conn.Release()
		return true
	})
}

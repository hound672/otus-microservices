package postgres

import (
	"context"
	"sync"

	"github.com/jackc/pgx/v4"

	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
	"gitlab.com/hound672/otus-microservices/06/cmd/tools"
)

type PsqlUoW struct {
	connectionManager *ConnectionManager
	log               logger.Logger
	tx                map[string]pgx.Tx
	m                 sync.Mutex
}

func NewPsqlUoW(connectionManager *ConnectionManager, log logger.Logger) *PsqlUoW {
	uow := &PsqlUoW{
		connectionManager: connectionManager,
		log:               log,
		tx:                make(map[string]pgx.Tx),
	}
	return uow
}

func (p *PsqlUoW) Start(ctx context.Context) error {
	conn, err := p.connectionManager.GetConnection(ctx)
	if err != nil {
		return err
	}

	reqID := tools.GetRequestID(ctx)
	tx, err := conn.Begin(ctx)
	if err != nil {
		return err
	}

	p.m.Lock()
	defer p.m.Unlock()
	p.tx[reqID] = tx

	return nil
}

func (p *PsqlUoW) Done(ctx context.Context, commit bool) {
	reqID := tools.GetRequestID(ctx)

	p.m.Lock()
	defer p.m.Unlock()

	tx, ok := p.tx[reqID]
	if !ok {
		p.log.Warnf("cannot find transaction for: %s", reqID)
		return
	}
	delete(p.tx, reqID)

	if commit {
		_ = tx.Commit(ctx)
	} else {
		_ = tx.Rollback(ctx)
	}
}

package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	loggerPkg "gitlab.com/hound672/otus-microservices/06/cmd/logger"
	configPkg "gitlab.com/hound672/otus-microservices/06/cmd/postgres/config"
)

func InitPsql(config *configPkg.Config, logger loggerPkg.Logger) (*pgxpool.Pool, func(), error) {
	ctx := context.Background()
	uri := fmt.Sprintf("postgres://%s:%s@%s:%d/%s",
		config.Username, config.Password, config.Host, config.Port, config.Database)

	pool, err := pgxpool.Connect(ctx, uri)
	if err != nil {
		logger.Errorf("Error psql connection: %v", err)
		return nil, nil, err
	}

	cleanup := func() {
		logger.Debugf("Close psql connection")
		pool.Close()
	}

	return pool, cleanup, nil
}

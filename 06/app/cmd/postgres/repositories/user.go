package repositories

import (
	"context"
	"gitlab.com/hound672/otus-microservices/06/cmd/postgres"
	"gitlab.com/hound672/otus-microservices/06/internal/models"
	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
)

func NewUser(connectionManager *postgres.ConnectionManager) repositories.User {
	user := &user{
		connectionManager: connectionManager,
	}
	return user
}

type user struct {
	connectionManager *postgres.ConnectionManager
}

func (r *user) Add(ctx context.Context, user *models.User) (*models.User, error) {
	conn, err := r.connectionManager.GetConnection(ctx)
	if err != nil {
		return nil, err
	}

	query := `
	INSERT INTO users (username, email, phone)
	VALUES ($1, $2, $3)
`
	_, err = conn.Exec(
		ctx, query, user.Username, user.Email, user.Phone,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *user) ReadAll(ctx context.Context) ([]*models.User, error) {
	conn, err := r.connectionManager.GetConnection(ctx)
	if err != nil {
		return nil, err
	}

	query := `
	SELECT id, username, email, phone
	FROM users
`

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []*models.User{}

	for rows.Next() {
		user := &models.User{}

		err = rows.Scan(&user.ID, &user.Username, &user.Email, &user.Phone)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}

func (r *user) ReadByID(ctx context.Context, userID int) (*models.User, error) {
	conn, err := r.connectionManager.GetConnection(ctx)
	if err != nil {
		return nil, err
	}

	query := `
	SELECT id, username, email, phone
	FROM users
	WHERE id=$1
`

	rows, err := conn.Query(ctx, query, userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	if !rows.Next() {
		return nil, repositories.ErrUserNotFound
	}

	user := &models.User{}
	err = rows.Scan(&user.ID, &user.Username, &user.Email, &user.Phone)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *user) Update(ctx context.Context, user *models.User) error {
	conn, err := r.connectionManager.GetConnection(ctx)
	if err != nil {
		return err
	}

	query := `
	UPDATE users SET username=$2, email=$3, phone=$4
	WHERE id=$1
`

	_, err = conn.Exec(
		ctx, query, user.ID, user.Username, user.Email, user.Phone,
	)
	if err != nil {
		return err
	}

	return nil
}

func (r *user) Delete(ctx context.Context, user *models.User) error {
	conn, err := r.connectionManager.GetConnection(ctx)
	if err != nil {
		return err
	}

	query := `
	DELETE FROM users WHERE id=$1
`
	_, err = conn.Exec(
		ctx, query, user.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

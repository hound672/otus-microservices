-- +migrate Up
CREATE TABLE users
(
    id     SERIAL PRIMARY KEY,
    username VARCHAR(255),
    email VARCHAR(255),
    phone VARCHAR(10)
);


-- +migrate Down
DROP TABLE users;

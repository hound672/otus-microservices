package migrations

import (
	migrate "github.com/rubenv/sql-migrate"

	"gitlab.com/hound672/otus-microservices/06/cmd/application"
)

func Up(app *application.Application) error {
	app.Logger.Infof("Start UP migrations")

	migration, err := prepare(app)
	if err != nil {
		return err
	}

	n, err := migration.set.Exec(
		migration.db, dialect, migration.source, migrate.Up,
	)
	if err != nil {
		return err
	}

	app.Logger.Infof("Accepted %d migrations", n)

	return nil
}

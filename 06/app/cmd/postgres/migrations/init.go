package migrations

import (
	"database/sql"
	"fmt"

	"github.com/gobuffalo/packr/v2"
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/rubenv/sql-migrate" //nolint

	"gitlab.com/hound672/otus-microservices/06/cmd/application"
)

const (
	tableName = "migrations"
	dir       = "./versions"
	dialect   = "postgres"
)

type migration struct {
	db     *sql.DB
	source *migrate.PackrMigrationSource
	set    migrate.MigrationSet
}

func prepare(app *application.Application) (*migration, error) {
	config := app.AppConfig.Postgres
	uri := fmt.Sprintf("postgres://%s:%s@%s:%d/%s",
		config.Username, config.Password, config.Host, config.Port, config.Database)

	db, err := sql.Open("pgx", uri)
	if err != nil {
		return nil, err
	}

	Box := packr.New("migrations", dir)
	source := &migrate.PackrMigrationSource{
		Box: Box,
	}
	set := migrate.MigrationSet{
		TableName: tableName,
	}

	migration := &migration{
		db:     db,
		source: source,
		set:    set,
	}

	return migration, nil
}

package migrations

import (
	"gitlab.com/hound672/otus-microservices/06/cmd/application"
)

func Status(app *application.Application) error {
	app.Logger.Infof("Start UP migrations")

	migration, err := prepare(app)
	if err != nil {
		return err
	}

	n, err := migration.set.GetMigrationRecords(
		migration.db, dialect,
	)
	if err != nil {
		return err
	}

	app.Logger.Infof("==== ACCEPTED MIGRATIONS ====")
	for _, record := range n {
		app.Logger.Infof("ID: %s; AppliedAt: %v", record.Id, record.AppliedAt)
	}
	app.Logger.Infof("=============================")

	return nil
}

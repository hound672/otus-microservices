package cli

import (
	"gitlab.com/hound672/otus-microservices/06/cmd/application"
	"gitlab.com/hound672/otus-microservices/06/cmd/postgres/migrations"
)

func appInit() {
	app, stop, err := application.CreateApplication()
	if err != nil {
		handleErrorInitApp(err)
	}
	defer stop()

	err = migrations.Up(app)
	if err != nil {
		app.Logger.Fatalf("Error up migrate: %v", err)
	}

	app.Logger.Infof("App initialized")
}

package cli

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func Main() {
	var rootCmd = &cobra.Command{
		Use:     "otus",
		Version: "0.0.0",
	}

	var initCmd = &cobra.Command{
		Use:   "init",
		Short: "app init",
		Long:  "application initialization",
		Run: func(cmd *cobra.Command, args []string) {
			appInit()
		},
	}

	var runCmd = &cobra.Command{
		Use:   "run",
		Short: "run otus application",
		Long:  "Run otus application",
		Run: func(cmd *cobra.Command, args []string) {
			run()
		},
	}

	var migrateRootCmd = &cobra.Command{
		Use:   "migrate",
		Short: "migrate database",
		Long:  "migrate database root command",
	}

	var migrateUpCmd = &cobra.Command{
		Use:   "up",
		Short: "migrate database up",
		Long:  "database up migration",
		Run: func(cmd *cobra.Command, args []string) {
			migrateUp()
		},
	}

	var migrateDownCmd = &cobra.Command{
		Use:   "down",
		Short: "migrate database down",
		Long:  "database down migration",
		Run: func(cmd *cobra.Command, args []string) {
			migrateDown()
		},
	}

	var migrateStatusCmd = &cobra.Command{
		Use:   "status",
		Short: "migrate database info",
		Long:  "print current migration status",
		Run: func(cmd *cobra.Command, args []string) {
			migrateStatus()
		},
	}

	rootCmd.AddCommand(initCmd, runCmd, migrateRootCmd)
	rootCmd.CompletionOptions.DisableDefaultCmd = true
	migrateRootCmd.AddCommand(migrateUpCmd, migrateDownCmd, migrateStatusCmd)
	err := rootCmd.Execute()
	if err != nil {
		handleErrorInitApp(err)
	}
}

func handleErrorInitApp(err error) {
	fmt.Printf("Error init app: %v\n", err)
	os.Exit(1)
}

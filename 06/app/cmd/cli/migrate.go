package cli

import (
	"gitlab.com/hound672/otus-microservices/06/cmd/application"
	"gitlab.com/hound672/otus-microservices/06/cmd/postgres/migrations"
)

func migrateUp() {
	app, stop, err := application.CreateApplication()
	if err != nil {
		handleErrorInitApp(err)
	}
	defer stop()

	if err := migrations.Up(app); err != nil {
		app.Logger.Fatalf("Error up migrate: %v", err)
	}
}

func migrateDown() {
	app, stop, err := application.CreateApplication()
	if err != nil {
		handleErrorInitApp(err)
	}
	defer stop()

	if err := migrations.Down(app); err != nil {
		app.Logger.Fatalf("Error down migrate: %v", err)
	}
}

func migrateStatus() {
	app, stop, err := application.CreateApplication()
	if err != nil {
		handleErrorInitApp(err)
	}
	defer stop()

	if err := migrations.Status(app); err != nil {
		app.Logger.Fatalf("Error obtain migrations status: %v", err)
	}
}

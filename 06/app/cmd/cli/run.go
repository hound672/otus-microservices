package cli

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/hound672/otus-microservices/06/cmd/application"
)

func waitQuitSignal() {
	quit := make(chan os.Signal)
	//nolint
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
}

func run() {
	app, stop, err := application.CreateApplication()
	if err != nil {
		handleErrorInitApp(err)
	}
	defer stop()

	if err := app.Run(); err != nil {
		app.Logger.Fatalf("Error run application: %v", err)
	}
	waitQuitSignal()
}

package config

import (
	"os"

	"github.com/spf13/viper"

	httpConfigPkg "gitlab.com/hound672/otus-microservices/06/cmd/http/config"
	loggerConfigPkg "gitlab.com/hound672/otus-microservices/06/cmd/logger"
	psqlConfigPkg "gitlab.com/hound672/otus-microservices/06/cmd/postgres/config"
)

func InitConfig(configFile string) (*AppConfig, error) {
	viper.SetConfigFile(configFile)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	for _, k := range viper.AllKeys() {
		value := viper.GetString(k)
		if value == "" {
			continue
		}
		viper.Set(k, os.ExpandEnv(value))
	}

	// http config
	viperHTTPServer := viper.Sub("http_server")
	httpConfig := &httpConfigPkg.Config{
		Port: viperHTTPServer.GetInt("port"),
	}

	// logger config
	viperLogger := viper.Sub("logger")
	loggerConfig := &loggerConfigPkg.Config{
		Level: viperLogger.GetString("level"),
	}

	// psql config
	viperPsql := viper.Sub("postgres")
	psqlConfig := &psqlConfigPkg.Config{
		Host:     viperPsql.GetString("host"),
		Port:     viperPsql.GetInt("port"),
		Username: viperPsql.GetString("username"),
		Password: viperPsql.GetString("password"),
		Database: viperPsql.GetString("database"),
	}

	appConfig := &AppConfig{
		HTTPServer: httpConfig,
		Logger:     loggerConfig,
		Postgres:   psqlConfig,
	}
	return appConfig, nil
}

package config

import (
	httpConfig "gitlab.com/hound672/otus-microservices/06/cmd/http/config"
	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
	psqlConfig "gitlab.com/hound672/otus-microservices/06/cmd/postgres/config"
)

type AppConfig struct {
	HTTPServer *httpConfig.Config
	Logger     *logger.Config
	Postgres   *psqlConfig.Config
}

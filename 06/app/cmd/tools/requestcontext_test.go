package tools

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewRequestContext(t *testing.T) {
	ctx := NewRequestContext(context.Background())
	ctxVal := ctx.Value(reqID)

	require.NotNil(t, ctxVal)

	reqID, ok := ctxVal.(string)
	require.True(t, ok)
	require.NotEmpty(t, reqID)
}

func TestGetRequestID(t *testing.T) {
	ctx := NewRequestContext(context.Background())
	reqID := ctx.Value(reqID).(string)

	require.Equal(t, reqID, GetRequestID(ctx))
}

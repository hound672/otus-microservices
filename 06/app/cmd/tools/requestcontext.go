package tools

import (
	"context"

	"github.com/google/uuid"
)

const (
	reqID = "req_id"
)

func NewRequestContext(ctx context.Context) context.Context {
	newCtx := context.WithValue(ctx, reqID, uuid.NewString()) //nolint
	return newCtx
}

func GetRequestID(ctx context.Context) string {
	return ctx.Value(reqID).(string)
}

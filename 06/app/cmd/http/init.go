package http

import (
	"context"
	"time"

	prometheusEcho "github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"

	controllersPkg "gitlab.com/hound672/otus-microservices/06/cmd/http/controllers"
	loggerPkg "gitlab.com/hound672/otus-microservices/06/cmd/logger"
	"gitlab.com/hound672/otus-microservices/06/internal/services"
)

const shutdownServerTimeout = 10

func InitHTTPServer(
	logger loggerPkg.Logger,
	controllers *controllersPkg.Controllers,
	requestContext services.RequestContext,
) (*echo.Echo, func(), error) {
	e := echo.New()
	initRoutes(e, controllers)
	initMiddlewares(e, requestContext)
	initValidator(e)

	p := prometheusEcho.NewPrometheus("otus_crud", nil)
	p.Use(e)

	cleanup := func() {
		logger.Debugf("Shutdown server...")
		ctx, cancel := context.WithTimeout(context.Background(), shutdownServerTimeout*time.Second)
		defer cancel()

		if err := e.Shutdown(ctx); err != nil {
			logger.Warnf("Server forced to shutdown:", err)
		}
	}

	return e, cleanup, nil
}

package consts

const (
	AuthSessionCookieName = "AUTH_SESS"
	UserSessionCookieName = "USER_SESS"

	JWTHeaderName        = "Authorization"
	JWTBearerName        = "Bearer"
	RetryAfterHeaderName = "Retry-After"
)

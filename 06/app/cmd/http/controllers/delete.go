package controllers

import (
	"errors"
	"github.com/labstack/echo/v4"
	errors2 "gitlab.com/hound672/otus-microservices/06/cmd/http/controllers/errors"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/dtoes"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/tools"
	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
	"gitlab.com/hound672/otus-microservices/06/internal/stories"
)

type Delete struct {
	story *stories.Delete
}

func NewDelete(story *stories.Delete) *Delete {
	deleteS := &Delete{
		story: story,
	}
	return deleteS
}

func (ctrl *Delete) Handle(c echo.Context) error {
	ctx := c.Request().Context()

	userParams := &dtoes.UserParams{}
	if err := (&echo.DefaultBinder{}).BindPathParams(c, userParams); err != nil {
		return err
	}
	user := &dtoes.UserCreateDTO{}
	if err := tools.InputValidator(c, user); err != nil {
		return err
	}

	err := ctrl.story.Handle(ctx, userParams.UserID)
	switch {
	case errors.Is(err, repositories.ErrUserNotFound):
		return errors2.ErrUserNotFound
	case err == nil:
		break
	default:
		return err
	}

	return nil

}

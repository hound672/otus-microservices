package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/hound672/otus-microservices/06/cmd/http/controllers/errors"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/dtoes"
	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
)

type ErrorHandler struct {
	log logger.Logger
}

const (
	UnknownErrorCode = "UNKNOWN_ERROR"
)

func NewErrorHandler(log logger.Logger) *ErrorHandler {
	handler := &ErrorHandler{
		log: log,
	}
	return handler
}

func (ctrl *ErrorHandler) Handle(err error, c echo.Context) {
	var statusCode int
	var code interface{}
	var details interface{}

	switch he := err.(type) {
	case *echo.HTTPError:
		statusCode = he.Code
		code = he.Message
	case *errors.ErrValidation:
		statusCode = he.Code
		code = he.Message
		details = he.Details
	default:
		statusCode = http.StatusInternalServerError
		code = UnknownErrorCode
		ctrl.log.Errorf("unexpected error. url: %s; err: %v",
			c.Request().URL, err,
		)
	}

	errorResponse := &dtoes.CommonErrorCDTO{
		Code:    code,
		Details: details,
	}
	_ = c.JSON(statusCode, errorResponse)
}

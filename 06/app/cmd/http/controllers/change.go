package controllers

import (
	"errors"
	"github.com/labstack/echo/v4"
	errors2 "gitlab.com/hound672/otus-microservices/06/cmd/http/controllers/errors"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/dtoes"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/tools"
	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
	"gitlab.com/hound672/otus-microservices/06/internal/repositories"
	"gitlab.com/hound672/otus-microservices/06/internal/stories"
)

type Change struct {
	log   logger.Logger
	story *stories.Change
}

func NewChange(
	log logger.Logger,
	story *stories.Change,
) *Change {
	change := &Change{
		log:   log,
		story: story,
	}
	return change
}

func (ctrl *Change) Handle(c echo.Context) error {
	ctx := c.Request().Context()

	userParams := &dtoes.UserParams{}
	if err := (&echo.DefaultBinder{}).BindPathParams(c, userParams); err != nil {
		return err
	}
	user := &dtoes.UserCreateDTO{}
	if err := tools.InputValidator(c, user); err != nil {
		return err
	}

	err := ctrl.story.Handle(ctx, userParams.UserID, user.Username, user.Email, user.Phone)
	switch {
	case errors.Is(err, repositories.ErrUserNotFound):
		return errors2.ErrUserNotFound
	case err == nil:
		break
	default:
		return err
	}

	return nil
}

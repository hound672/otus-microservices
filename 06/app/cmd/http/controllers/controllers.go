package controllers

type Controllers struct {
	*ErrorHandler
	*Create
	*Change
	*Delete
	*Get
}

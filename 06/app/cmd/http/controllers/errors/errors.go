package errors

import (
	"gitlab.com/hound672/otus-microservices/06/cmd/http/dtoes"
	"net/http"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
)

var (
	ErrServiceUnavailable = &echo.HTTPError{Code: http.StatusServiceUnavailable, Message: "SERVICE_UNAVAILABLE"}
	ErrTooManyRequests    = &echo.HTTPError{Code: http.StatusTooManyRequests, Message: "TOO_MANY_REQUESTS"}
	ErrValidate           = &echo.HTTPError{Code: http.StatusBadRequest, Message: "VALIDATION_ERROR"}
	ErrUserNotFound       = &echo.HTTPError{Code: http.StatusNotFound, Message: "USER_NOT_FOUND"}
)

// Validation error

type ErrValidation struct {
	*echo.HTTPError
	Details *dtoes.ValidationErrorCDTO
}

func (err *ErrValidation) As(other interface{}) bool {
	v, ok := other.(**echo.HTTPError)
	if !ok {
		return false
	}
	*v = err.HTTPError
	return ok
}

func GetErrValidation(err error) error {
	v, ok := err.(validator.ValidationErrors)
	if !ok {
		return err
	}

	e := v[0]
	details := &dtoes.ValidationErrorCDTO{
		Path:      e.Field(),
		Validator: e.Tag(),
	}

	return &ErrValidation{
		HTTPError: ErrValidate,
		Details:   details,
	}
}

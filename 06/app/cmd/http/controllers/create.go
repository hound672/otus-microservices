package controllers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/dtoes"
	"gitlab.com/hound672/otus-microservices/06/cmd/http/tools"
	"gitlab.com/hound672/otus-microservices/06/internal/stories"
	"net/http"

	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
)

type Create struct {
	log   logger.Logger
	story *stories.Create
}

func NewCreate(
	log logger.Logger,
	story *stories.Create,
) *Create {
	create := &Create{
		log:   log,
		story: story,
	}
	return create
}

func (ctrl *Create) Handle(c echo.Context) error {
	ctx := c.Request().Context()

	user := &dtoes.UserCreateDTO{}
	if err := tools.InputValidator(c, user); err != nil {
		return err
	}

	err := ctrl.story.Handle(ctx, user.Username, user.Email, user.Phone)
	if err != nil {
		return err
	}

	return tools.SuccessResponse(c, http.StatusCreated, nil)
}

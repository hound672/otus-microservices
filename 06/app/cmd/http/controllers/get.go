package controllers

import (
	"gitlab.com/hound672/otus-microservices/06/cmd/http/dtoes"
	"math/rand"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	"gitlab.com/hound672/otus-microservices/06/cmd/http/tools"
	"gitlab.com/hound672/otus-microservices/06/cmd/logger"
	"gitlab.com/hound672/otus-microservices/06/internal/stories"
)

type Get struct {
	log   logger.Logger
	story *stories.Get
}

func NewGet(
	log logger.Logger,
	story *stories.Get,
) *Get {
	get := &Get{
		log:   log,
		story: story,
	}
	return get
}

func (ctrl *Get) Handle(c echo.Context) error {
	ctx := c.Request().Context()

	delay := rand.Intn(1000)
	ctrl.log.Infof("rand: %v", delay)
	time.Sleep(time.Millisecond * time.Duration(delay))

	users, err := ctrl.story.Handle(ctx)
	if err != nil {
		return err
	}

	usersDto := []*dtoes.UserGetDTO{}

	for _, user := range users {
		userDto := &dtoes.UserGetDTO{
			ID: user.ID,
			UserCreateDTO: dtoes.UserCreateDTO{
				Username: user.Username,
				Email:    user.Email,
				Phone:    user.Phone,
			},
		}
		usersDto = append(usersDto, userDto)
	}

	return tools.SuccessResponse(c, http.StatusOK, usersDto)
}

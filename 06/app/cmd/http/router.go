package http

import (
	"github.com/labstack/echo/v4"

	controllersPkg "gitlab.com/hound672/otus-microservices/06/cmd/http/controllers"
)

const (
	UserCreateURL = "/user"
	UserGetURL    = "/user"
	UserChangeURL = "/user/:user_id"
	UserDeleteURL = "/user/:user_id"
)

func initRoutes(e *echo.Echo, controllers *controllersPkg.Controllers) {
	e.HTTPErrorHandler = controllers.ErrorHandler.Handle

	e.POST(UserCreateURL, controllers.Create.Handle)
	e.GET(UserGetURL, controllers.Get.Handle)
	e.PUT(UserChangeURL, controllers.Change.Handle)
	e.DELETE(UserDeleteURL, controllers.Delete.Handle)
}

package http

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/hound672/otus-microservices/06/internal/services"
)

func NewRequestSetupMiddleware(
	requestContext services.RequestContext,
) func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			newCtx := requestContext.NewContext(c.Request().Context())
			c.SetRequest(c.Request().WithContext(newCtx))
			return next(c)
		}
	}
}

func NewRequestTeardownMiddleware(
	requestContext services.RequestContext,
) func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			res := next(c)
			requestContext.Teardown(c.Request().Context())
			return res
		}
	}
}

//nolint
func initMiddlewares(e *echo.Echo, requestContext services.RequestContext) {
	e.Use(NewRequestSetupMiddleware(requestContext))
	e.Use(NewRequestTeardownMiddleware(requestContext))
}

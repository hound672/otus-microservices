package dtoes

type UserCreateDTO struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}

type UserGetDTO struct {
	UserCreateDTO
	ID int `json:"id"`
}

type UserParams struct {
	UserID int `param:"user_id"`
}

package dtoes

type CommonErrorCDTO struct {
	Code    interface{} `json:"code"`
	Details interface{} `json:"details,omitempty"`
}

type ValidationErrorCDTO struct {
	Path      string `json:"path"`
	Validator string `json:"validator"`
}

package tools

import (
	"github.com/labstack/echo/v4"
)

func SuccessResponse(c echo.Context, statusCode int, data interface{}) error {
	if data == nil {
		return c.NoContent(statusCode)
	}
	return c.JSON(statusCode, data)
}

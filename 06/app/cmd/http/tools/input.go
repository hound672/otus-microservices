package tools

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/hound672/otus-microservices/06/cmd/http/controllers/errors"
)

func InputValidator(c echo.Context, data interface{}) error {
	if err := c.Bind(data); err != nil {
		return errors.GetErrValidation(err)
	}
	if err := c.Validate(data); err != nil {
		return errors.GetErrValidation(err)
	}
	return nil
}
